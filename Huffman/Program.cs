﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Huffman
{

    // Priority Queue
    public class Node
    {
        public char Symbol { get; set; }
        public int Frequency { get; set; }
        public Node Right { get; set; }
        public Node Left { get; set; }

        // recursively encodes a given character into List<int>
        public List<int> Traverse(char symbol, List<int> data)
        {
            // this is a leaf node, if the symbols match return the data
            if (Right == null && Left == null)
            {
                if (symbol.Equals(Symbol))
                    return data;                   
                else
                    return null;
            }
            // otherwise, we need to traverse the child nodes
            else
            {
                List<int> left = null;
                List<int> right = null;

                if (Left != null)
                {
                    // create a new List of this childs encoding
                    List<int> leftPath = new List<int>();
                    // add each data element to the encoding
                    foreach (var elem in data) leftPath.Add(elem);
                    // all left child nodes receive a 0
                    leftPath.Add(0);
                    // recursively traverse the child node
                    left = Left.Traverse(symbol, leftPath);
                }

                if (Right != null)
                {
                    List<int> rightPath = new List<int>();
                    foreach (var elem in data) rightPath.Add(elem);
                    // all right child nodes receive a 1
                    rightPath.Add(1);
                    // recursively traverse this child node
                    right = Right.Traverse(symbol, rightPath);
                }

                if (left != null)
                    return left;
                else
                    return right;
            }
        }
    }

    public class HuffmanTree
    {
        private List<Node> nodes = new List<Node>();
        public Node Root { get; set; }
        public Dictionary<char, int> CharFrequency = new Dictionary<char, int>();
        public Dictionary<char, List<int>> EncodedDict = new Dictionary<char, List<int>>();

        public void Analyze(string inputString)
        {
            // builds string into Dictionary based on frequency
            foreach (char elem in inputString)
            {
                if (CharFrequency.ContainsKey(elem))
                    CharFrequency[elem]++;
                else
                    CharFrequency[elem] = 1;
            }

            // adds each key value pair in CharFrequency to the Node<List>
            foreach (var item in CharFrequency)
                nodes.Add(new Node() { Symbol = item.Key, Frequency = item.Value });

            // constructs the priority queue by combining child node frequencies into parent nodes
            BuildTree();  
        }

        private void BuildTree()
        {
            // sorts the nodes based on frequency and set Root node
            while (nodes.Count > 1)
            {
                // orders nodes by frequency ascending
                List<Node> orderedNodes = nodes.OrderBy(node => node.Frequency).ToList<Node>();

                if (orderedNodes.Count >= 2)
                {
                    // take the first two items from the ordered list
                    List<Node> items = orderedNodes.Take(2).ToList<Node>();
                    // create parent node of these two nodes and combine their frequencies
                    Node parent = new Node()
                    {
                        Symbol = '*',
                        Frequency = items[0].Frequency + items[1].Frequency,
                        Left = items[0],
                        Right = items[1]
                    };
                    nodes.Remove(items[0]);
                    nodes.Remove(items[1]);
                    nodes.Add(parent);
                }
                Root = nodes.First();
            }

        }

        // encodes the values from the sorted priority queue
        public List<int> Encode(string source)
        {
            List<int> encodedList = new List<int>();
            // iterates each character in the string
            foreach (char symbol in source)
            {
                // returns List<bool> to represent the strings encoded bits
                List<int> charSymbol = Root.Traverse(symbol, new List<int>());

                // add the encoded bits to the master list
                foreach (var value in charSymbol)
                    encodedList.Add(value);

                // add symbol and encoded bits to dictionary
                // this is done to record a 'Encoding Chart' for testing and display purposes
                if (!EncodedDict.ContainsKey(symbol))
                    EncodedDict.Add(symbol, charSymbol);
            }

            return encodedList;
        }

        // takes an encoded array and decodes it back to a normal string
        public string Decode(List<int> encodedList)
        {
            Node current = Root;
            string decoded = "";

            foreach (int elem in encodedList)
            {
                // it is a 1 bit
                if (elem == 1)
                {
                    // check right, if it is a sub-tree - traverse it
                    if (current.Right != null)
                        current = current.Right;
                }
                // it is a 0 bit
                else
                {
                    // check left, if it is a sub-tree, traverse it
                    if (current.Left != null)
                        current = current.Left;
                }

                // otherwise we are at a leaf node, add the symbol to the string and continue from Root
                if (current.Left == null && current.Right == null)
                {
                    decoded += current.Symbol;
                    current = Root;
                }
            }

            return decoded;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<string> testInputs = new List<string>
            {
                // every letter in the alphabet once
                "the quick onyx goblin jumps over the lazy dwarf",

                // a general sentence
                "this is an example for huffman encoding",

                // both upper and lower case characters from the same words
                "MIXed mixED CAse caSE INPut inpUT teST TEst hEre HeRE",

                // every one of the first seven letters once
                "abcdefg",

                // first letter 4 times, second letter 3 times, third letter 2 times, fourth letter once
                "aaaabbbccd",

                // first letter once, second letter 2 times, third letter 3 times, fourth letter 4 times
                "abbcccdddd",

                // no letters, empty string
                "",
            };

            foreach (var input in testInputs)
            {
                HuffmanTree tree = new HuffmanTree();

                Console.WriteLine($"Input String: '{input}'");

                // Build the Huffman tree
                tree.Analyze(input);

                // Encode
                List<int> encoded = tree.Encode(input);
                Console.Write("Encoded: ");
                foreach (int bit in encoded)
                {
                    Console.Write(bit);
                }
                Console.WriteLine();

                // print the encoded key/value pairs
                foreach (var pair in tree.EncodedDict)
                {
                    Console.Write($"{pair.Key}: ");
                    foreach (var item in pair.Value)
                    {
                        Console.Write(item.ToString());
                    }
                    Console.WriteLine();
                }

                // Decode
                string decoded = tree.Decode(encoded);
                Console.WriteLine($"Decoded: '{decoded}'");
                Console.WriteLine(input == decoded ? "Encoding & Decoding Success!" : "Encoding & Decoding Failure!");
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}
